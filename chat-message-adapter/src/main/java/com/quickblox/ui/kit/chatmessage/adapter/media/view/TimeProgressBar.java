package com.quickblox.ui.kit.chatmessage.adapter.media.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.TimeBar;

/**
 * Created by roman on 8/4/17.
 */

public class TimeProgressBar extends ProgressBar implements TimeBar {

    private long duration;
    private long position;
    private boolean isPaused = false;
    private Player player;

    private final Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updatePosition();
        }
    };

    public TimeProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void pauseProgress() {
        isPaused = true;
    }

    public void playProgress(Player _player) {
        isPaused = false;
        player = _player;
        updatePosition();
    }

    @Override
    public void setEnabled(boolean enabled) {

    }

    @Override
    public void setListener(OnScrubListener listener) {

    }

    @Override
    public void setKeyTimeIncrement(long time) {

    }

    @Override
    public void setKeyCountIncrement(int count) {

    }

    @Override
    public void setPosition(long position) {
        this.position = position;
//        updatePosition(position, false);

//        if(position > 0) {
//            animation = ObjectAnimator.ofInt(this, "progress", (int)(position));
//            animation.setDuration(1000);
//            animation.setInterpolator(new LinearInterpolator());
//            animation.start();
//        }
//        else {
//            this.position = position;
//        }
    }

    private void updatePosition() {
        if(isPaused || player == null) {
            removeCallbacks(updateProgressAction);
            return;
        }
        this.setProgress((int)player.getCurrentPosition());

        removeCallbacks(updateProgressAction);
        postDelayed(updateProgressAction, 50);
    }

    @Override
    public void setBufferedPosition(long bufferedPosition) {

    }

    @Override
    public void setDuration(long duration) {
        this.duration = duration;
        this.setMax((int)duration);
        updateProgress();
    }

    @Override
    public void setAdGroupTimesMs(@Nullable long[] adGroupTimesMs, @Nullable boolean[] playedAdGroups, int adGroupCount) {

    }

    private void updateProgress() {
        int scrubberPixelPosition = 0;
        if(duration != 0) {
            scrubberPixelPosition = (int) ((getMax() * position) / duration);
        }
        setProgress(scrubberPixelPosition);
    }
}
