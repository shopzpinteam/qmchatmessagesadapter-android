package com.quickblox.ui.kit.chatmessage.adapter.utils;

import android.content.res.Resources;

/**
 * Created by noelchew on 18/01/2018.
 */

public class PixelUtil {
    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
